### mod_note

This module is based on `mod_env` of `Apache` and it offers setting custom entries in request notes.


```
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{NoteCommon}n\" \"%{NoteCustomized}n\" \"%{NoteOnly}n\"" common

# these stuffs are global
SetNote NoteCommon "this is a common note"
SetNote NoteCustomized "this note should be customized"

<VirtualHost *>
  ServerName foobar1
  ...

  SetNote NoteCustomized "this note was customized by vhost 1"
  SetNote NoteOnly "this note is set in vhost1 config only"
</VirtualHost>


<VirtualHost *>
  ServerName foobar2
  ...

  SetNote NoteCustomized "this note was customized by vhost 2"
</VirtualHost>
```
