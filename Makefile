# Makefile for mod_note.c (gmake)
# $Id: Makefile 16 2007-12-13 03:40:22Z thomas $
APXS=./apxs.sh

note: mod_note.so
	@echo make done
	@echo type \"make install\" to install mod_note

mod_note.so: mod_note.c
	$(APXS) -c -n $@ mod_note.c

mod_note.c:

install: mod_note.so
	$(APXS) -i -S LIBEXECDIR=$(DESTDIR)$$($(APXS) -q LIBEXECDIR)/ -n mod_note.so mod_note.la

clean:
	rm -rf *~ *.o *.so *.lo *.la *.slo *.loT .libs/
